const readline = require('readline-sync');
const answer = readline.question('Give a year.');

const modulo4 = answer % 4;
const modulo100 = answer % 100;
const modulo400 = answer % 400;

if (modulo4 === 0 && modulo100 !== 0) {
  console.log('Year is a leap year.');
}

if (modulo4 === 0 && modulo100 === 0 && modulo400 === 0) {
  console.log('Year is a leap year.');
} else {
  console.log('Year is not a leap year.');
}
