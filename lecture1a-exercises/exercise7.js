const xor = (a, b) => {
  if ((a && !b) || (!a && b)) return true;
  else return false;
};

const conditionA = true;
const conditionB = false;
const conditionC = true;
const conditionD = false;

console.log(xor(conditionA, conditionB));
console.log(xor(conditionA, conditionC));
console.log(xor(conditionA, conditionD));
