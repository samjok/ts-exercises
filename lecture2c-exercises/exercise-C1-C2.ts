// Exercise C1
const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];

const newArr = arr.filter((nmb) => nmb % 3 === 0);
console.log(newArr);

// Exercise C2
const duplicateEliminator = (someArray: string[]) => {
  let filteredArray: string[] = [];
  for (let i = 0; i < someArray.length; i++) {
    if (filteredArray.includes(someArray[i])) {
      continue;
    } else filteredArray.push(someArray[i]);
  }
  return filteredArray;
};