const readline = require('readline-sync');

const words: string[] = [
  'team',
  'fish',
  'meal',
  'seal',
  'sale',
  'bird',
  'golf',
];
const index = Math.floor(Math.random() * 7);
const word = words[index];
const wordArray = [...word];
let isRight = false;

while (!isRight) {
  let guess: string = readline.question('Your guess: ').toLowerCase();
  let bulls = 0;
  let cows = 0;
  for (let i = 0; i < word.length; i++) {
    if (word.charAt(i) === guess.charAt(i)) {
      bulls++;
    } else if (wordArray.includes(guess.charAt(i))) {
      cows++;
    } else continue;
  }
  console.log(`Bulls: ${bulls}`);
  console.log(`Cows: ${cows}`);

  if (bulls === 4) {
    isRight = true;
  }
}

console.log(`Word was ${word}`);
