let deck: string[] = new Array(52);

const numbers = [
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  '10',
  'J',
  'Q',
  'K',
  'A',
];
const suits = ['Heart', 'Club', 'Diamond', 'Spade'];
let deckIndex = 0;

for (let i = 0; i < 4; i++) {
  for (let j = 0; j < 14; j++) {
    deck[deckIndex] = `${suits[i]} ${numbers[j]}`;
    deckIndex++;
  }
}

const shuffle = (deck: string[]) => {
  let currentIndex = deck.length;
  let temporaryValue, randomIndex;

  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex = currentIndex - 1;
    temporaryValue = deck[currentIndex];
    deck[currentIndex] = deck[randomIndex];
    deck[randomIndex] = temporaryValue;
  }
};

const takeCard = (deck: string[]) => {
  const card = deck[0];
  deck.splice(0, 1);
  console.log(card);
};

takeCard(deck);

console.log(deck);
