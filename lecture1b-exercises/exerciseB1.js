let sum = 0;
const max = 10;

for (let a = 0; a < max; a++) {
  if (a % 5 === 0 || a % 3 === 0) {
    sum = sum + a;
  }
}

console.log(sum);
