const readline = require('readline-sync');
const answer = readline.question('When were you born?');

const today = new Date();
const year = today.getFullYear();

console.log(`Your age is ${year - answer}.`);
