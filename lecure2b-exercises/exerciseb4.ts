const randomArrayCreator = () => {
  const randomArray: Array<number> = new Array(20);
  for (let i = 0; i < 20; i++) {
    randomArray[i] = Math.floor(Math.random() * 100);
  }
  randomArray.sort();
  console.log('sorted', randomArray);
};

const randomArrayCreator2 = () => {
  const randomArray: Array<number> = new Array(20);
  for (let i = 0; i < 20; i++) {
    randomArray[i] = Math.floor(Math.random() * 100);
  }
  randomArray.reverse();
  console.log('reversed', randomArray);
};