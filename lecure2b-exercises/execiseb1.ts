const modifier = (str: String) => {
  const trimmed = str.trim().toLowerCase();
  if (trimmed.length > 20) {
    return trimmed.substring(0, 20);
  } else return trimmed;
};