import fs from 'fs';

const library = [
  {
    author: 'David Wallace',
    title: 'Infinite Jest',
    readingStatus: false,
    id: 1,
  },
  {
    author: 'Douglas Hofstadter',
    title: 'Gödel, Escher, Bach',
    readingStatus: true,
    id: 2,
  },
  {
    author: 'Harper Lee',
    title: 'To Kill A Mockingbird',
    readingStatus: false,
    id: 3,
  },
];

// #1 - getBook()
const getBook = (id: number) => {
  const book = library.find((book) => book.id === id);
  return book;
};

// #2 - printBookData
const printBookData = (id: number) => {
  const book = library.find((book) => book.id === id);
  console.log(book);
};

// #3 - printReadingStatus
const printReadingStatus = (author: string, title: string) => {
  const book = library.find(
    (book) => book.author === author && book.title === title
  );
  console.log(book?.readingStatus);
};

// #4 - addNewBook
const addNewBook = (author: string, title: string) => {
  const lastId = library[library.length - 1].id;
  const newBook = {
    author: author,
    title: title,
    readingStatus: false,
    id: lastId + 1,
  };
  library.push(newBook);
};

// #5 - readBook
const readBook = (id: number) => {
  const book = library.find((book) => book.id === id);
  if (book) {
    book.readingStatus = true;
  } else console.log('Book not found.');
};

// #6 - saveToJSON
const saveToJSON = () => {
  fs.writeFileSync('library.json', JSON.stringify(library), 'utf8');
};

// #7 - loadFromJSON
const loadFromJSON = () => {
  const loadedLibrary = fs.readFileSync('library.json', 'utf8');
  const parsedData = JSON.parse(loadedLibrary);
  return parsedData;
};
