const factorial = (number: Number) => {
  let sum = 1;
  for (let a = 1; a <= number; a++) {
    sum = sum * a;
    console.log(a, 'sum:', sum);
  }
  console.log(`Factorial of number ${number} is`, sum);
};
