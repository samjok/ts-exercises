import React from 'react';
import '../styles.css';
import { INotes } from '../interfaces';

const Notes: React.FC<INotes> = (props) => {
  return (
    <div className='noteTable'>
      {props.notes.map((note, index) => (
        <div className='note'>
          <div className='titleBar'>
            <div className='twoThird'>
              <div className='noteTitle'>
                <strong>{note.title}</strong>
              </div>
            </div>
            <div className='oneThird'>
              <button
                className='btn'
                onClick={(event) => {
                  event.preventDefault();
                  props.deleteNote(index);
                }}
              >
                Delete
              </button>
            </div>
          </div>
          <div className='noteText'>{note.message}</div>
          <div className='noteSignature'>
            <strong>{note.signature}</strong>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Notes;
