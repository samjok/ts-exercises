import React, { useState } from 'react';
import TextField from './TextField';
import Notes from './Notes';
import { INote } from '../interfaces';
import '../styles.css';

const MainPage: React.FC = () => {
  const [notes, setNotes] = useState<INote[]>([]);
  const addNote = (note: INote): void => {
    if (note) {
      const noteArray = [...notes];
      noteArray.push(note);
      setNotes(noteArray);
    }
  };
  const deleteNote = (index: number) => {
    const newArray = [...notes];
    newArray.splice(index, 1);
    setNotes(newArray);
  };
  return (
    <div className='container'>
      <div className='titleBar'>
        <h1>Note App 0.0.1</h1>
      </div>
      <div className='rowContainer'>
        <div className='halfpage'>
          <div className='column'>
            <TextField addNote={addNote} />
          </div>
        </div>
        <div className='halfpage'>
          <div className='column'>
            <Notes notes={notes} deleteNote={deleteNote} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default MainPage;
