import React, { useState } from 'react';
import '../styles.css';
import { ITextField } from '../interfaces';

const TextField: React.FC<ITextField> = (props) => {
  const [title, setTitle] = useState<string>('');
  const [message, setMessage] = useState<string>('');
  const [signature, setSignature] = useState<string>('');

  const handleMessageChange = (event: any) => {
    setMessage(event.target.value);
  };
  const handleTitleChange = (event: any) => {
    setTitle(event.target.value);
  };
  const handleSignatureChange = (event: any) => {
    setSignature(event.target.value);
  };
  const resetFields = () => {
    setMessage('');
    setTitle('');
    setSignature('');
  };
  return (
    <div>
      <form className='textForm'>
        Title:
        <input value={title} onChange={handleTitleChange}></input>
        <br />
        Message:
        <textarea
          className='textField'
          value={message}
          onChange={handleMessageChange}
        ></textarea>
        <br />
        Signature:
        <input value={signature} onChange={handleSignatureChange}></input>
        <div className='buttonRow'>
          <button
            className='btn'
            onClick={(event) => {
              event.preventDefault();
              props.addNote({
                title: title,
                message: message,
                signature: signature,
              });
              resetFields();
            }}
          >
            Submit
          </button>
        </div>
      </form>
    </div>
  );
};

export default TextField;
