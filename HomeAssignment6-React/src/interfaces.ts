export interface INote {
  title: string;
  message: string;
  signature: string;
}

export interface INotes {
  notes: INote[];
  deleteNote: (index: number) => void;
}

export interface ITextField {
  addNote: (note: INote) => void;
}
