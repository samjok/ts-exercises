const readline = require('readline-sync');
const values = [];
const value1 = readline.question('Give a number: ');
const value2 = readline.question('Give another number: ');
const value3 = readline.question('Give one more: ');
values.push(value1);
values.push(value2);
values.push(value3);

const sorted = values.sort();

console.log('Smallest number is ', sorted[0]);
console.log('Largest number is ', sorted[2]);
