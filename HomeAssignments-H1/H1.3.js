const readline3 = require('readline-sync');
const number2 = readline3.question('Give a number: ');

if (number2 > 0) {
  const squareRoot = Math.sqrt(number2);
  console.log(`Square root of a number ${number2} is ${squareRoot}.`);
} else {
  console.log('ERROR! You gave a negative number.');
}
