const readline2 = require('readline-sync');
const number1 = readline2.question('Give a number between 0 and 10: ');

if (number1 <= 10 && number1 >= 0) {
  console.log(`You gave a number ${number1}.`);
} else {
  console.log('ERROR! That number is out of the range!');
}
